# Test Case - 008 - Enforce Project-Level Scan Execution Policy for chosen branches with SAST scan to run on automatically selected runners.

### Result: ✅ success


### Description

Enforce Project-Level Scan Execution Policy to be enforced for pipelines for chosen branches with SAST scan to run on automatically selected runners.

### Video
![Recorded Demo](videos/scan-execution-policies/008-enforce-project-level-scan-execution-for-pipelines-chosen-branches-with-sast-on-automatically-selected-runners.mp4)

### Epics
* https://gitlab.com/groups/gitlab-org/-/epics/6586+

### Issues
* https://gitlab.com/gitlab-org/gitlab/-/issues/340722+
* https://gitlab.com/gitlab-org/gitlab/-/issues/340458+

## Preparation
1. Create new Project in namespace with Ultimate license (`test-scenario-XXX`)
1. Add `.gitlab-ci.yml` file to this project:
   ```yml
   test-job:
     script:
     - echo "Test Job..."
   ```
1. Add `run_os_script.rb` file to this project:
   ```ruby
   class Generic
     def run_on_system
       system("ls")
     end
   end
   ```

## Steps

1. Go to `Security & Compliance` and click on `Policies`
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies` page is loaded with empty list of policies and link to the documentation
      * `This project is not linked to a security policy project` message is visible with `New policy` button
      * `Edit policy project` button is visible
      * `New policy` button is visible
      * `Type` dropdown is visible with `All policies` option pre-selected
      * `Source` dropdown is visible with `All policies` option pre-selected
   
   </details>

1. Click on `New Policy button` (try if for both buttons performed action is the same)
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new` page is loaded with 2 possible policy types to select
      * `Scan result policy` is visible to be selected with description, example and `Select policy` button
      * `Scan execution policy` is visible to be selected with description, example and `Select policy` button
      * `Cancel` button is visible
   
   </details>

1. Click on `Select policy` button in `Scan execution policy` section
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new?type=scan_execution_policy` page is loaded
      * Switch between `Rule mode` and `.yaml mode` is visible
      * Breadcrumbs `Step 1: Choose a policy type` and `Step 2: Policy details` are visible
      * Empty `Name` field is visible
      * Empty optional `Description` field is visible
      * `Policy status` switch is visible with 2 options `Enabled` and `Disabled` where `Enabled` is preselected
      * `Conditions` section is visible with rule: IF A pipeline is run actions for the * branches
      * `+ Add condition` link is visible under `Conditions` section
      * `Actions` section is visible with action: THEN Require a DAST scan to run with site profile [empty field] and scanner profile [empty field] on runnar that has specific tag with ability to `Select runner tags` and ? icon with tooltip that presents information `If the field is empty, the runner will be automatically selected`
      * `+ Add action` link is visible under `Actions` section
      * `Cancel` button is visible
      * Disabled `Configure with a merge request` button is visible and not clickable
      * `.yaml preview` is visible with content:
         ```yaml
         type: scan_execution_policy
         name: ''
         description: ''
         enabled: true
         rules:
         - type: pipeline
           branches:
           - '*'
         actions:
         - scan: dast
           site_profile: ''
           scanner_profile: ''
           tags: []
         ```
   
   </details>

1. Fill `Name` with `Test 008`
   <details>
   <summary>Expected results</summary>

      * `Configure with a merge request` button is active
      * `.yaml preview` is updated with new name
   
   </details>

1. In `Actions` section change the scan to SAST.
   <details>
   <summary>Expected results</summary>

      * Fields `Site profile` and `Scanner profile` are not visible
      * `Actions` section is visible with action: THEN Require a SAST scan to run on runner that `has specific tag` with ability to `Select runner tags` and ? icon with tooltip that presents information `If the field is empty, the runner will be automatically selected`
      * `.yaml preview` is visible with content:
        ```yaml
        type: scan_execution_policy
        name: 'Test 008'
        description: ''
        enabled: true
        rules:
        - type: pipeline
          branches:
          - '*'
        actions:
        - scan: sast
          tags: []
        ```
   
   </details>

1. In `Conditions` section modify `*` branches to `main`.
   <details>
   <summary>Expected results</summary>

      * Fields in `Condition` branch set to `main`
      * `.yaml preview` is visible with content:
        ```yaml
        type: scan_execution_policy
        name: 'Test 008'
        description: ''
        enabled: true
        rules:
        - type: pipeline
          branches:
          - main
        actions:
        - scan: sast
          tags: []
        ```
   
   </details>

1. In `Actions` section modify `has specifig tag` and set `selected automatically`.
   <details>
   <summary>Expected results</summary>

      * Field in `Actions` `has specific tag` modified to `selected automatically`
      * `.yaml preview` is visible with content:
        ```yaml
        type: scan_execution_policy
        name: 'Test 008'
        description: ''
        enabled: true
        rules:
        - type: pipeline
          branches:
          - main
        actions:
        - scan: sast
          tags: []
        ```
   
   </details>

1. Click on `Configure with a merge request` button
   <details>
   <summary>Expected results</summary>

      * New Project is created `test-008 - Security policy project` (`test-008-security-policy-project`)
      * New MR is created in this project with `Update scan policies` title
      * No assignees/reviewers/labels/milestone is set
      * In `Changes` tab `.gitlab/security-policies/policy.yml` is visible with YAML content
   
   </details>

1. Click on `Merge` button
   <details>
   <summary>Expected results</summary>

      * The merge request is merged.
   
   </details>

1. Go back to the main project (`test-008`), go to `CI/CD` -> `Pipelines` and click `Run pipeline` button
   <details>
   <summary>Expected results</summary>

      * New page `/-/pipelines/new` is loaded with ability to run pipeline with variables for `main` branch
   
   </details>

1. Click on `Run pipeline` button
   <details>
   <summary>Expected results</summary>

      * `test-job` job is visible
      * `brakeman-sast-0` job is visible
   
   </details>

1. Click on `brakeman-sast-0` job
   <details>
   <summary>Expected results</summary>

      * The job has `success` status.
   
   </details>

