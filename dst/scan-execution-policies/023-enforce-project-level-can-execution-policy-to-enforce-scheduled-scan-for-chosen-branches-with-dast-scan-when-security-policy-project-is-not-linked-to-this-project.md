# Test Case - 023 - Enforce Project-Level Scan Execution Policy to schedule scan for chosen branches with DAST scan when Security Policy Project is not linked.

### Result: ✅ success


### Description

Create a Scan Execution Policy on the Project-Level to enforce scheduled scan for chosen branches with DAST scan when the Security Policy Project is not linked to this project.

### Video
![Recorded Demo](videos/scan-execution-policies/023-enforce-project-level-can-execution-policy-to-enforce-scheduled-scan-for-chosen-branches-with-dast-scan-when-security-policy-project-is-not-linked-to-this-project.mp4)

### Epics

### Issues

## Preparation
1. Create new Project in namespace with Ultimate license (`test-scenario-XXX`)
1. Add `.gitlab-ci.yml` file to this project:
   ```yml
   test-job:
     script:
     - echo "Test Job..."
   ```

## Steps

1. Go to `Security & Compliance` and click on `Policies`
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies` page is loaded with empty list of policies and link to the documentation
      * `This project is not linked to a security policy project` message is visible with `New policy` button
      * `Edit policy project` button is visible
      * `New policy` button is visible
      * `Type` dropdown is visible with `All policies` option pre-selected
      * `Source` dropdown is visible with `All policies` option pre-selected
   
   </details>

1. Click on `New Policy button` (try if for both buttons performed action is the same)
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new` page is loaded with 2 possible policy types to select
      * `Scan result policy` is visible to be selected with description, example and `Select policy` button
      * `Scan execution policy` is visible to be selected with description, example and `Select policy` button
      * `Cancel` button is visible
   
   </details>

1. Click on `Select policy` button in `Scan execution policy` section
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new?type=scan_execution_policy` page is loaded
      * Switch between `Rule mode` and `.yaml mode` is visible
      * Breadcrumbs `Step 1: Choose a policy type` and `Step 2: Policy details` are visible
      * Empty `Name` field is visible
      * Empty optional `Description` field is visible
      * `Policy status` switch is visible with 2 options `Enabled` and `Disabled` where `Enabled` is preselected
      * `Conditions` section is visible with rule: IF A pipeline is run actions for the * branches
      * `+ Add condition` link is visible under `Conditions` section
      * `Actions` section is visible with action: THEN Require a DAST scan to run with site profile [empty field] and scanner profile [empty field] on runnar that has specific tag with ability to `Select runner tags` and ? icon with tooltip that presents information `If the field is empty, the runner will be automatically selected`
      * `+ Add action` link is visible under `Actions` section
      * `Cancel` button is visible
      * Disabled `Configure with a merge request` button is visible and not clickable
      * `.yaml preview` is visible with content:
         ```yaml
         type: scan_execution_policy
         name: ''
         description: ''
         enabled: true
         rules:
         - type: pipeline
           branches:
           - '*'
         actions:
         - scan: dast
           site_profile: ''
           scanner_profile: ''
           tags: []
         ```
   
   </details>

1. Fill `Name` with `Scheduled a scan for DAST on the main branch`
   <details>
   <summary>Expected results</summary>

      * `Configure with a merge request` button is active
      * `.yaml preview` is updated with new name
   
   </details>

1. In `Conditions` section change the IF condition to `Schedule` .
   <details>
   <summary>Expected results</summary>

      * The actions for dropdown apprears with a selection of `branch` or `agent`
      * Schedule dropdowns appear with a selection of `daily` and `weekly` and a dropdown for the time of the day.
      * `.yaml preview` is visible with content:
        ```yaml
          type: scan_execution_policy
          name: Scheduled scan for DAST on main branch
          description: ''
          enabled: true
          rules:
            - type: schedule
              branches: []
              cadence: 0 0 * * *
          actions:
            - scan: dast
              site_profile: ''
              scanner_profile: ''
              tags: []
        ```
   
   </details>

1. Fill the `Select branches` field with `main`.
   <details>
   <summary>Expected results</summary>

      * `.yaml preview` is visible with content:
        ```yaml
          type: scan_execution_policy
          name: ''
          description: ''
          enabled: true
          rules:
            - type: schedule
              branches:
                - main
              cadence: 0 0 * * *
          actions:
            - scan: dast
              site_profile: ''
              scanner_profile: ''
              tags: []
        ```
   
   </details>

1. In `Actions` section change the THEN condition to `DAST`.
   <details>
   <summary>Expected results</summary>

      * A dropdown for the scanner profile appears
      * A dropdown for the site profile appears
      * `.yaml preview` is visible with content:
        ```yaml
          type: scan_execution_policy
          name: Scheduled scan for DAST on main branch
          description: ''
          enabled: true
          rules:
            - type: schedule
              branches:
                - main
              cadence: 0 0 * * *
          actions:
            - scan: dast
              site_profile: ''
              scanner_profile: ''
              tags: []
        ```
   
   </details>

1. Click `Select scanner profile`.
   <details>
   <summary>Expected results</summary>

      * The side menu `Scanner profile library` appears.
   
   </details>

1. Select `New scanner profile`.
   <details>
   <summary>Expected results</summary>

      * The `New scanner profile` form appears.
   
   </details>

1. Fill `Profile name` with `test` and select `Save profile`
   <details>
   <summary>Expected results</summary>

      * The side menu form disappears.
      * The dropdown for `scanner profile` now says `test`.
      * `.yaml preview` is visible with content:
        ```yaml
          type: scan_execution_policy
          name: Scheduled scan for DAST on main branch
          description: ''
          enabled: true
          rules:
            - type: schedule
              branches:
                - main
              cadence: 0 0 * * *
          actions:
            - scan: dast
              tags: []
              site_profile: ''
              scanner_profile: test
        ```
   
   </details>

1. Click `Select site profile`.
   <details>
   <summary>Expected results</summary>

      * The side menu `Site profile library` appears.
   
   </details>

1. Select `New site profile`.
   <details>
   <summary>Expected results</summary>

      * The `New site profile` form appears.
   
   </details>

1. Fill `Profile name` with `test`, `Target URL` with `https://example.com` and select `Save profile`
   <details>
   <summary>Expected results</summary>

      * The side menu form disappears.
      * The dropdown for `site profile` now says `test`.
      * `.yaml preview` is visible with content:
        ```yaml
          type: scan_execution_policy
          name: Scheduled scan for DAST on main branch
          description: ''
          enabled: true
          rules:
            - type: schedule
              branches:
                - main
              cadence: 0 0 * * *
          actions:
            - scan: dast
              tags: []
              site_profile: test
              scanner_profile: test
        ```
   
   </details>

1. Click on `Configure with a merge request` button
   <details>
   <summary>Expected results</summary>

      * New Project is created `test-scenario-XXX - Security policy project` (`test-scenario-XXX-security-policy-project`)
      * New MR is created in this project with `Update scan policies` title
      * No assignees/reviewers/labels/milestone is set
      * In `Changes` tab `.gitlab/security-policies/policy.yml` is visible with YAML content
   
   </details>

1. Click on `Merge` button
   <details>
   <summary>Expected results</summary>

      * The merge request is merged.
   
   </details>

