# Test Case - 013 - Enforce License scan matching new types targeting All Branches

### Result: ✅ success


### Description

Enforce Project-Level Scan Result Policy when License Scan finds any license matching selected newly detected license types in an open merge request targeting All Branches.

### Video
![Recorded Demo](videos/scan-result-policies/013-enforce-license-scan-matching-new-for-all-branches.mp4)

### Epics
* https://gitlab.com/groups/gitlab-org/-/epics/8092+

### Issues

## Preparation
1. Create new Project in namespace with Ultimate license (`test-scenario-XXX`)
1. Add `.gitlab-ci.yml` file to this project:
   ```yml
   include:
   - template: Jobs/License-Scanning.gitlab-ci.yml
   test-job:
     script:
       - echo "Test Job..."
    ```
1. Add `requirements.txt` file to this project:
    ```
    osrframework
    ```

## Steps

1. Go to `Security & Compliance` and click on `Policies`
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies` page is loaded with empty list of policies and link to the documentation
      * `This project is not linked to a security policy project` message is visible with `New policy` button
      * `Edit policy project` button is visible
      * `New policy` button is visible
      * `Type` dropdown is visible with `All policies` option pre-selected
      * `Source` dropdown is visible with `All policies` option pre-selected
   
   </details>

1. Click on `New Policy button` (try if for both buttons performed action is the same)
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new` page is loaded with 2 possible policy types to select
      * `Scan result policy` is visible to be selected with description, example and `Select policy` button
      * `Scan execution policy` is visible to be selected with description, example and `Select policy` button
      * `Cancel` button is visible
   
   </details>

1. Click on `Select policy` button in `Scan result policy` section
   <details>
   <summary>Expected results</summary>

      * `/-/security/policies/new?type=scan_result_policy` page is loaded
      * Switch between `Rule mode` and `.yaml mode` is visible
      * Breadcrumbs `Step 1: Choose a policy type` and `Step 2: Policy details` are visible
      * Empty `Name` field is visible
      * Empty optional `Description` field is visible
      * `Policy status` switch is visible with 2 options `Enabled` and `Disabled` where `Enabled` is preselected
      * `Rules` section is visible with rule: IF Select scan type
      * `+ Add rule` link is visible under `Rules` section
      * `Actions` section is visible with action: THEN Require approval from 1 of the following approvers with ability to `Search users or groups`
      * `Cancel` button is visible
      * Disabled `Configure with a merge request` button is visible and not clickable
      * `.yaml preview` is visible with content:
         ```yaml
         type: scan_result_policy
         name: ''
         description: ''
         enabled: true
         rules:
           - type: ''
         actions:
           - type: require_approval
             approvals_required: 1
             user_approvers: []
         ```
   
   </details>

1. Fill `Name` with `Test 013`
   <details>
   <summary>Expected results</summary>

      * `Configure with a merge request` button is active
      * `.yaml preview` is updated with new name
   
   </details>

1. In `Rules` section, change the scan type to `License Scan`
   <details>
   <summary>Expected results</summary>

      * Fields `matching`, `license type`, `select license states` and `All protected branches` become visible
   
   </details>

1. In `Rules` section, select `matching`, select `Python License 2.0` name and select `Newly-detected`
   <details>
   <summary>Expected results</summary>

      * `Actions` section is visible with action: THEN Require approval from 1 of the following approvers with ability to `Search users or groups`
      * `.yaml preview` is visible with content:
        ```yaml
        type: scan_result_policy
        name: Test 013
        description: ''
        enabled: true
        rules:
          - type: license_finding
            branches: []
            match_on_inclusion: true
            license_types:
              - Python License 2.0
            license_states:
              - newly_detected
        actions:
          - type: require_approval
            approvals_required: 1
            user_approvers: []
        ```
   
   </details>

1. In `Actions` sections, select `Groups` from `Choose approver type`, then select the `Govern Security Policies Group` group
   <details>
   <summary>Expected results</summary>

      * `.yaml preview` is visible with content:
        ```yaml
        ...
        actions:
          - type: require_approval
            approvals_required: 1
            group_approvers_ids:
              - 64024513
        ```
   
   </details>

1. Click on `Configure with a merge request` button
   <details>
   <summary>Expected results</summary>

      * New Project is created `test-013 - Security policy project` (`test-013-security-policy-project`)
      * New MR is created in this project with `Update scan policies` title
      * No assignees/reviewers/labels/milestone is set
      * In `Changes` tab `.gitlab/security-policies/policy.yml` is visible with YAML content
   
   </details>

1. Click on `Merge` button
   <details>
   <summary>Expected results</summary>

      * The merge request is merged.
   
   </details>

1. Update `requirements.txt` file in this project, create a new branch and commit:'
    ```
    osrframework
    matplotlib
    ```
   <details>
   <summary>Expected results</summary>

      * New page -/merge_requests/new is loaded with ability to create a new MR with the changed file
   
   </details>

1. Create merge request
   <details>
   <summary>Expected results</summary>

      * Merge request is created
      * Pipeline is running
   
   </details>

1. Click on the running pipeline and wait for it to finish
   <details>
   <summary>Expected results</summary>

      * `license_scanning` job is visible and successful
      * `test-job` job is visible and successful
   
   </details>

1. Click on `Licenses` tab
   <details>
   <summary>Expected results</summary>

      * `License Compliance detected X licenses and policy violations for the source branch only` is displayed
      * `Denied` is displayed
      * `Out-of-compliance with this project's policies and should be removed` is displayed
      * `Python license 2.0 Used by matplotlib` is displayed
   
   </details>

1. Go back to the created MR
   <details>
   <summary>Expected results</summary>

      * 1 approval is required from the defined group
      * A message is displayed with `License Compliance detected X new licenses and policy violations; approval required`
      * Merge is blocked due to missing approvals
   
   </details>

