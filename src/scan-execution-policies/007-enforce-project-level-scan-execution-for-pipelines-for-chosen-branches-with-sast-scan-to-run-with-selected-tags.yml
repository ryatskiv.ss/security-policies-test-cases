---
title: Enforce Project-Level Scan Execution for pipelines for chosen branches with SAST scan to run with selected tags.
description: Enforce Project-Level Scan Execution Policy to be enforced for pipelines for chosen branches with SAST scan to run on runners with selected runner tags.
preparation:
- partial: preparation/new-project-with-single-ci-job
- partial: preparation/new-safe-ruby-file
steps:
- partial: steps/001-go-to-security-compliance-and-click-policies
- partial: steps/002-click-on-new-policy-button
- partial: steps/003-click-on-scan-execution-policy-section
- partial: steps/general/fill-policy-name
  replace:
    POLICY_NAME: Test 007
- step: In `Actions` section change the scan to SAST.
  expected_results:
  - Fields `Site profile` and `Scanner profile` are not visible
  - "`Actions` section is visible with action: THEN Require a SAST scan to run on runner that `has specific tag` with ability to `Select runner tags` and ? icon with tooltip that presents information `If the field is empty, the runner will be automatically selected`"
  - |-
    `.yaml preview` is visible with content:
            ```yaml
            type: scan_execution_policy
            name: 'Test 007'
            description: ''
            enabled: true
            rules:
            - type: pipeline
              branches:
              - '*'
            actions:
            - scan: sast
              tags: []
            ```
- step: In `Conditions` section modify `*` branches to `main`.
  expected_results:
  - Fields `Site profile` and `Scanner profile` are not visible
  - |-
    `.yaml preview` is visible with content:
            ```yaml
            type: scan_execution_policy
            name: 'Test 007'
            description: ''
            enabled: true
            rules:
            - type: pipeline
              branches:
              - main
            actions:
            - scan: sast
              tags: []
            ```
- step: In `Actions` section modify `has specifig tag` to use one of the selected ones (`shared`).
  expected_results:
  - Field in `Actions` `has specific tag` changed to `shared`
  - |-
    `.yaml preview` is visible with content:
            ```yaml
            type: scan_execution_policy
            name: 'Test 007'
            description: ''
            enabled: true
            rules:
            - type: pipeline
              branches:
              - main
            actions:
            - scan: sast
              tags:
              - shared
            ```
- partial: steps/general/configure-with-a-merge-button
  replace:
    PROJECT_NAME: test-007
- partial: steps/general/merge
- partial: steps/general/run-pipeline
  replace:
    PROJECT_NAME: test-007
    BRANCH_NAME: main
- step: Click on `Run pipeline` button
  expected_results:
  - "`test-job` job is visible"
  - "`brakeman-sast-0` job is visible"
- step: Click on `brakeman-sast-0` job
  expected_results:
  - The job has `success` status.
video: videos/scan-execution-policies/007-enforce-project-level-scan-execution-for-pipelines-for-chosen-branches-with-sast-scan-to-run-with-selected-tags.mp4
epics:
- https://gitlab.com/groups/gitlab-org/-/epics/6586
issues:
- https://gitlab.com/gitlab-org/gitlab/-/issues/340722
- https://gitlab.com/gitlab-org/gitlab/-/issues/340458